# Changelog ICTShop Dropship

* Added   : untuk penambahan fitur.
* Changed : untuk perubahan pada menu yang sudah ada atau optimasi.
* Removed : untuk menghapus fitur.
* Fixed   : untuk perbaikan bug atau error.

## [3.4.0.4] - 2018-02-13
### Menu Penjualan Barang
* **Added :**
* - Cetak faktur penjualan menggunakan sistem load data.
* - Cetak surat jalan menggunakan sistem load data.
* - Cetak faktur sudah ada beberapa jenis yaitu A5, 58mm dan 76mm.
* **Changed :**
* - Optimasi menu.
### Menu Laporan Penjualan Barang
* **Added :**
* - Cetak faktur penjualan menggunakan sistem load data.
* - Cetak surat jalan menggunakan sistem load data.
* - Cetak faktur sudah ada beberapa jenis yaitu A5, 58mm dan 76mm.
* **Changed :**
* - Optimasi menu.
* - Defatult awal pencarian data berdasarkan tanggal hari ini.
